const express = require('express');
const bodyparser = require('body-parser');
require("dotenv").config();
require('./Config/db');

var app = express();
var cors = require('cors');
//Configuring express server
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

// enabled cors 
app.use(cors());

// inside public directory.
app.use(express.static('public')); 

const PORT = process.env.PORT || 5000;

http = require('http');
const server = http.createServer(app);
server.listen(PORT, () => 'Server is running on port '+PORT);    

var apiRouter = require('./Routes/api.routes');

app.use('/api', apiRouter);
   
// catch 404 and forward to error handler
app.use(function(req, res, next) 
{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.status(404).json({
      message: "No such route exists"
    })
});