const request = require('postman-request');


function get_latitude_longitude(citi) 
{
    return new Promise(function (resolve, reject)
    {
        
        $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyAze2Vkj0ZoO03Xlw03L9eimoGM3KCz0cI&address="+citi+"";
        let options = {
                     'method': 'GET',
                     'url': $url,
                     'headers': {
                        'Authorization': process.env.AUTHORIZATION_TOKEN
                      }
                };

                request(options, function (error, response) 
                    {
                        if (error)
                        {
                            reject(error);
                        } else {
                            resolve(response.body);
                        }
                    });
    })
}

function groupo_api_call(latitude,longitude,limit,keyword)
{
    return new Promise(function(resolve, reject)
    {
        let key = keyword.replace(' ', '+');
        let urlGo = '';
        if(key != '')
        {
            console.log('key1', key);
            if(key == 'restaurants')
            {
                urlGo = "https://www.groupon.com/occasion/deals-json?filterQuery=(subcategory:"+key+")&context=web_holiday&showBestOption=true&divisionLoc="+latitude+","+longitude+"&divisionLocale=en_US&pageType=holiday&apiFacets=topcategory%7Ccategory%7Ccategory2%7Ccategory3%2Cdeal_type&sort=price:desc&includeHtml=true&offset=0&limit="+limit+"";
            } else {
                urlGo ="https://www.groupon.com/occasion/deals-json?filterQuery=(deal_type:groupon-deals-2987)&context=web_getaways&showBestOption=true&divisionLoc="+latitude+","+longitude+"&divisionLocale=en_US&pageType=getaways&includeHtml=true&offset=0&limit="+limit+"";
            }
        } else {
            urlGo ="https://www.groupon.com/occasion/deals-json?filterQuery=(deal_type:groupon-deals-2987)%20AND%20context=web_getaways&showBestOption=true&divisionLoc=41.184,-96.15&divisionLocale=en_US&pageType=getaways&includeHtml=true&offset=0&limit="+limit+"";
        }
        request(urlGo, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })  
}


module.exports = {
    get_latitude_longitude,
    groupo_api_call
    
}