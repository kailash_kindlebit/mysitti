const request = require('postman-request');

function get_filter_category(city,keyword,limit) 
{
    return new Promise(function (resolve, reject)
    {
        let key = keyword.replace(' ', '+');
        let urlgo = 'https://app.ticketmaster.com/discovery/v2/events?apikey=RfcE2uw4xpNzXAWjzvXzA5xxiKuuU9G4&keyword='+keyword+'&sort=date,asc&countryCode=US&city='+city+'&page=0';
        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });   
    })
}


module.exports = {
    get_filter_category
    
}