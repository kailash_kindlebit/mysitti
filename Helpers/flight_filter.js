const request = require('postman-request');
const md5 = require('md5');

function get_tickets_for_specific_date(origin,destination,departure_at,return_at,token) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'https://api.travelpayouts.com/aviasales/v3/prices_for_dates?origin='+origin+'&destination='+destination+'&departure_at='+departure_at+'&return_at='+return_at+'&unique=false&sorting=price&direct=false&currency=USD&limit=30&page=1&one_way=true&token='+token+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response.body);
            }
        });   
    })
}

function get_calendar_of_prices(origin,destination,show_to_affiliates,token) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'http://api.travelpayouts.com/v2/prices/month-matrix?currency=usd&origin='+origin+'&destination='+destination+'&show_to_affiliates='+show_to_affiliates+'&token='+token+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response.body);
            }
        });   
    })
}

function get_cheapest_tickets(origin,destination,depart_date,return_date,token) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'http://api.travelpayouts.com/v1/prices/cheap?origin='+origin+'&destination='+destination+'&depart_date='+depart_date+'&return_date='+return_date+'&token='+token+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response.body);
            }
        });   
    })
}

function get_non_stop_tickets(origin,destination,depart_date,return_date,token) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'http://api.travelpayouts.com/v1/prices/direct?origin='+origin+'&destination='+destination+'&depart_date='+depart_date+'&return_date='+return_date+'&token='+token+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response.body);
            }
        });   
    })
}

function get_flights_special_offers(origin,destination,airline,locale,token) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'https://api.travelpayouts.com/aviasales/v3/get_special_offers?origin='+origin+'&destination='+destination+'&airline='+airline+'&locale=en&token='+token+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response.body);
            }
        });   
    })
}

module.exports = {
    get_tickets_for_specific_date,
    get_calendar_of_prices,
    get_cheapest_tickets,
    get_non_stop_tickets,
    get_flights_special_offers
}