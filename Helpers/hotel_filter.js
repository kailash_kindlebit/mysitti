const request = require('postman-request');
const md5 = require('md5');


function get_hotel_searchid(iata,checkIn,checkOut,adultsCount,customerIP,childrenCount,childAge1,lang,currency,waitForResult,marker,signature,apiToken) 
{
    return new Promise(function (resolve, reject)
    {
        let urlgo = 'http://engine.hotellook.com/api/v2/search/start.json?iata='+iata+'&checkIn='+checkIn+'&checkOut='+checkOut+'&adultsCount='+adultsCount+'&customerIP='+customerIP+'&childrenCount='+childrenCount+'&childAge1='+childAge1+'&lang='+lang+'&currency='+currency+'&waitForResult='+waitForResult+'&marker='+marker+'&signature='+signature+'';        

        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });   
    })
}

function get_hotels(searchId,marker,apiToken) 
{
    return new Promise(function (resolve, reject)
    {
        let sign  = apiToken+':'+marker+':10:0:0:'+searchId+':1:price';
        let signature = md5(sign);
        let urlgo = 'http://engine.hotellook.com/api/v2/search/getResult.json?searchId='+searchId+'&limit=10&sortBy=price&sortAsc=1&roomsCount=0&offset=0&marker='+marker+'&signature='+signature+'';
                
        let options = {
            'method': 'GET',
            'url': urlgo,
            'headers': {
                'Authorization': process.env.AUTHORIZATION_TOKEN
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
                
            }
        });   
    })
}

module.exports = {
    get_hotel_searchid,
    get_hotels
    
}