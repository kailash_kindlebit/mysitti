const request = require('postman-request');



function get_latitude_longitude(citi) 
{
    return new Promise(function (resolve, reject)
    {
        // Do async job
        const city = !citi ? 'Chicago' : citi;
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+city+'&sensor=false&key=AIzaSyDS4E5J5Jg-A4g4piaCfjyqJ2rc4uU_dN4';
        request(url, async function (error, response, body) 
        {
            if (error) {
                reject(error);
            } else {
                let response = JSON.parse(body);
                resolve(response?.results[0]?.geometry?.location);
            }
        });
    })
}

function yelp_api_call(latitude,longitude,limit,keyword)
{
    return new Promise(function(resolve, reject)
    {
        let key = keyword.replace(' ', '+');
        let urlGo = limit != 0 ? 'https://api.yelp.com/v3/businesses/search?term='+key+'&latitude='+latitude+'&longitude='+longitude+'&limit='+limit+'' : 'https://api.yelp.com/v3/businesses/search?term='+key+'&latitude='+latitude+'&longitude='+longitude+'';
        
        let options = {
            'method': 'GET',
            'url': urlGo,
            'headers': {
                'Authorization': 'Bearer BjJKM-1ZSbav4VMbtIUvC4isdLkwrihG9XDUanCcbbknBWIXs1XHBbJnuzH5vgD0ETyCpxAg3FAvMvxB_z6QCnusskWwYEofgpkNvOY7ytK_HKGrGv-98bo44V-AWnYx'
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })  
}

// code for get All-Inclusive deals 
const get_all_inclusive = () => 
{
    return new Promise((resolve, reject) => 
    {
        var options = {
            'method': 'POST',
            'url': 'https://ads.api.cj.com/query',
            'headers': {
              'Authorization': 'Bearer 45agzktndc3f016pmtwwfdev51',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              query: `{ travelExperienceProducts(companyId: "4882762", partnerIds:"5840172",keywords: "Caribbean resorts", limit: 80 ) {totalCount,limit,resultList { advertiserId,advertiserName,categoryName, catalogId, id, title, description,sourceFeedType,imageLink,link,locationName, salePrice {amount,currency} price {amount, currency} linkCode(pid: "8265264") {clickUrl } }  } }`,
              variables: {}
            })
        };
        request(options, (error, response) =>
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

const get_all_tourForFun = (keywords,limit) => 
{
    return new Promise((resolve, reject) => 
    {
        var options = {
            'method': 'POST',
            'url': 'https://ads.api.cj.com/query',
            'headers': {
              'Authorization': 'Bearer 18czgj4wrymzfg4q6kfv6vxe01',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              query: `{
  travelExperienceProducts(companyId: "4882762",currency:"USD",lowPrice:4, keywords: "`+keywords+`", limit: `+limit+`) {
    totalCount
    limit
    resultList {
      advertiserId
      advertiserName
      categoryName
      catalogId
      id
      title
      description
      sourceFeedType
      imageLink
      link
      locationName
      salePrice {
        amount
        currency
      }
      price {
        amount
        currency
      }
      linkCode(pid: "8265264") {
        clickUrl
      }
    }
  }
}
`,
              variables: {}
            })
        };
        request(options, (error, response) =>
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

const get_all_inclusive_by_country = (keywords,limit) => 
{
    return new Promise((resolve, reject) => 
    {
        var options = {
            'method': 'POST',
            'url': 'https://ads.api.cj.com/query',
            'headers': {
              'Authorization': 'Bearer 45agzktndc3f016pmtwwfdev51',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              query: `{ travelExperienceProducts(companyId: "4882762", partnerIds:"5840172",keywords: "`+keywords+`", limit: `+limit+` ) {totalCount,limit,resultList { advertiserId,advertiserName,categoryName, catalogId, id, title, description,sourceFeedType,imageLink,link,locationName, salePrice {amount,currency} price {amount, currency} linkCode(pid: "8265264") {clickUrl } }  } }`,
              variables: {}
            })
        };
        request(options, (error, response) =>
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

module.exports = {
    get_latitude_longitude,
    yelp_api_call,
    get_all_inclusive,
    get_all_inclusive_by_country,
    get_all_tourForFun
}