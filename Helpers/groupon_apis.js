const request = require('postman-request');

function get_latitude_longitude(citi) 
{
    return new Promise(function (resolve, reject)
    {
        // Do async job
        const city = !citi ? 'Chicago' : citi;
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+city+'&sensor=false&key=AIzaSyDS4E5J5Jg-A4g4piaCfjyqJ2rc4uU_dN4';
        request(url, async function (error, response, body) 
        {
            if (error) {
                reject(error);
            } else {
                let response = JSON.parse(body);
                resolve(response?.results[0]?.geometry?.location);
            }
        });
    })
}

function groupo_api_call(latitude,longitude,limit,keyword)
{
    return new Promise(function(resolve, reject)
    {
        let key = keyword.replace(' ', '+');
        let urlGo = '';
        if(key != '')
        {
            console.log('key1', key);
            if(key == 'restaurants')
            {
                urlGo = "https://www.groupon.com/occasion/deals-json?filterQuery=(subcategory:"+key+")&context=web_holiday&showBestOption=true&divisionLoc="+latitude+","+longitude+"&divisionLocale=en_US&pageType=holiday&apiFacets=topcategory%7Ccategory%7Ccategory2%7Ccategory3%2Cdeal_type&sort=price:desc&includeHtml=true&offset=0&limit="+limit+"";
            } else {
                urlGo ="https://www.groupon.com/occasion/deals-json?filterQuery=(deal_type:groupon-deals-2987)&context=web_getaways&showBestOption=true&divisionLoc="+latitude+","+longitude+"&divisionLocale=en_US&pageType=getaways&includeHtml=true&offset=0&limit="+limit+"";
            }
        } else {
            urlGo ="https://www.groupon.com/occasion/deals-json?filterQuery=(deal_type:groupon-deals-2987)%20AND%20context=web_getaways&showBestOption=true&divisionLoc=41.184,-96.15&divisionLocale=en_US&pageType=getaways&includeHtml=true&offset=0&limit="+limit+"";
        }
        request(urlGo, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })  
}

module.exports = {
    get_latitude_longitude,
    groupo_api_call
}