const request = require('postman-request');



function get_zomato(url) 
{
  return new Promise(function (resolve, reject)
    {
       var options = {
            'method': 'GET',
            'url': url,
            'headers': {
              'Cache-Control': 'no-cache',
              "user-key": "99868269a38bfabc5532b10a32fa75c7"
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {

                resolve(response);
            }
        });   
    })
}

module.exports = {
    get_zomato
}