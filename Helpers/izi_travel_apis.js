const request = require('postman-request');


function izi_travel_api_call($url)
{
    return new Promise(function(resolve, reject)
    {
        var options = {
            'method': 'GET',
            'url': $url,
            'headers': {
              'Cache-Control': 'no-cache',
              'X-Izi-Api-Key': '3cabfbf6-f811-4249-b95e-d53a298672ac'
            }
        };

        request(options, function (error, response) 
        {
            if (error)
            {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

module.exports = {
    izi_travel_api_call
}