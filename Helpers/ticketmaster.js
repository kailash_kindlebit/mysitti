const request = require('postman-request');



function ticketmaster(keyword,city) 
{
      return new Promise(function (resolve, reject)
    {
        const interCity = city.replace(" ", "+")
        const interkeyword = keyword.replace(" ", "+")
        // let url = "https://app.ticketmaster.com/discovery/v2/events.json?apikey=aITXxLf5UxV3sX4Cie3KsqpkZAipeous&keyword="+keyword+"&city="+interCity+"&sort=date,asc&countryCode=US&page=0&startDateTime="
        let url = "https://app.ticketmaster.com/discovery/v2/events.json?keyword="+interkeyword+"&source=ticketmaster&apikey=aITXxLf5UxV3sX4Cie3KsqpkZAipeous&size=30"
        request(url, async function (error, response, body) 
        {
            if (error) {
                reject(error);
            } else {
                let response = JSON.parse(body);
                resolve(response);
            }
        });
    })
}

module.exports = {
    ticketmaster
}
