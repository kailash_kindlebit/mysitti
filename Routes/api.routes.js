const express = require('express');
const router = express.Router();
const apiController = require('../Controllers/api.controller');
const apiThingsTODoController = require('../Controllers/api.thingstodo.controller');
const dealsController = require('../Controllers/deals.controller');
const apihomecontroller = require('../Controllers/api.home.controller');
const flightController = require('../Controllers/flight.controller');
const hotelController = require('../Controllers/hotel.controller');
const familyController = require('../Controllers/family.controller');


router.get('/restaurant-deals-search', apiController.hotel_deals_search);  
router.get('/get-wp-blogs-with-filter', apiController.get_wp_blogs_with_filter);  
router.get('/flight-iata-code', apiController.flight_iata_code);  
router.post('/contact-us', apiController.contact_us);  
router.get('/get-guide-tours', apiController.get_guide_tours);  
router.get('/get-sightseeing-tours', apiController.get_sightseeing_tours);  
router.get('/zomato', apiController.zomatos);  
router.get('/ticketmaster', apiController.ticketmasters);  
router.get('/ticketmasters-sidebar', apiController.ticketmasters_sidebar);  
router.get('/yelp_sports-ticket', apiController.yelp_sports_ticket);  
router.get('/hotel-url', apiController.hotel_url);  
router.get('/flight-url', apiController.flight_url);  
router.get('/restaurant-deals-filter-search', apiController.hotel_deals_filter_search);
router.get('/get-izi-travel-apis', apiController.get_izi_travel_apis);
router.get('/car-rental', apiController.car_rental);
router.get('/all-tours', apiController.all_tours);
router.get('/beautiful-cities', apiController.beautiful_cities);
router.get('/get-izi-model-data', apiController.get_izi_model_data);
router.get('/specific-hotels', apiController.specific_hotels);
router.get('/get-beach', apihomecontroller.get_beach);
router.get('/cool-flight-deals', apihomecontroller.cool_flight_deals);

// blogs routes
router.get('/get-wp-blogs', apiController.get_wp_blogs);
router.get('/get-wp-single-blog', apiController.get_wp_single_blog);
router.get('/get-wp-single-blog-with-slug/:slug', apiController.get_wp_single_blog_with_slug);
router.get('/all-inclusive-deals', apiController.all_inclusive_deals);
                         
// things to do APIS
router.get('/get-things-local-music', apiThingsTODoController.get_local_music);
router.get('/get-family-top-attraction', apiThingsTODoController.get_family_top_attractions);
router.get('/get-winery', apiThingsTODoController.get_winery);
router.get('/get-escape-room', apiThingsTODoController.get_escape_room);
router.get('/get-comedy', apiThingsTODoController.get_comedy);
router.get('/get-adrenaline-rush', apiThingsTODoController.get_adrenaline_rush);
router.get('/get-tours-travel', apiThingsTODoController.get_tours_travel);
router.get('/get-dig-into', apiThingsTODoController.get_dig_into);
router.get('/get-where-to-stay', apiThingsTODoController.get_where_to_stay);
router.get('/get-sports-tickets', apiThingsTODoController.get_sports_tickets);
router.get('/get-groupon', apiThingsTODoController.get_groupon);
router.get('/get-ip', apiThingsTODoController.get_ip);
router.get('/get-yelp-deals', apiThingsTODoController.get_yelp_deals);
router.get('/get-all-tours', apiThingsTODoController.get_all_tours);
router.get('/category-filter', apiThingsTODoController.category_filter);


// deals APIS
router.get('/get-random-deals', dealsController.get_random_deals);

//hotel APIS
router.get('/hotel-filter', hotelController.hotel_filter);


// flights APIS
router.get('/get-tickets-for-specific-date', flightController.tickets_for_specific_date);
router.get('/get-calendar-of-prices-for-a-month', flightController.calendar_of_prices);
router.get('/get-cheapest-tickets', flightController.cheapest_tickets);
router.get('/get-non-stop-tickets', flightController.non_stop_tickets);
router.get('/get-flights-special-offers', flightController.flights_special_offers);

//Family Page

router.get('/get-family-groupon', familyController.groupon_api_call);
router.get('/get-tours-and-travel', familyController.tours_and_travel);


module.exports = router