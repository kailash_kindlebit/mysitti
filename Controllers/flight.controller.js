var mysqlConnection = require('../Config/db');
const flightFilter = require('../Helpers/flight_filter');


//Flight tickets for specific dates

const tickets_for_specific_date  = async(req,res) => 
{
    try {
        const page  = '1';
        const limit  = '30';
        const currency = 'USD';
        const sorting = 'price';
        const unique = false;
        const direct = false;
        const one_way = true;

        const {origin} = req.query;
        const {destination} = req.query;
        const {departure_at} = req.query;
        const {return_at} = req.query;

        const token =  process.env.API_TOKEN;

        const allInclusive = await flightFilter.get_tickets_for_specific_date(origin,destination,departure_at,return_at,token);
       
       res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

//The calendar of prices for a month

const calendar_of_prices  = async(req,res) => 
{
    try {
     
        const currency = 'USD';
        const {origin} = req.query;
        const {destination} = req.query;
        const show_to_affiliates = true;
        const token =  process.env.API_TOKEN;

        const allInclusive = await flightFilter.get_calendar_of_prices(origin,destination,show_to_affiliates,token);
       
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

//Cheapest tickets
const cheapest_tickets  = async(req,res) => 
{
    try {
        const {origin} = req.query;
        const {destination} = req.query;
        const {depart_date} = req.query;
        const {return_date} = req.query;

        const token =  process.env.API_TOKEN;

        const allInclusive = await flightFilter.get_cheapest_tickets(origin,destination,depart_date,return_date,token);
       
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

//  Non-stop tickets
const non_stop_tickets  = async(req,res) => 
{
    try {
        const {origin} = req.query;
        const {destination} = req.query;
        const {depart_date} = req.query;
        const {return_date} = req.query;

        const token =  process.env.API_TOKEN;

        const allInclusive = await flightFilter.get_non_stop_tickets(origin,destination,depart_date,return_date,token);
       
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

// Flights special offers

const flights_special_offers  = async(req,res) => 
{
    try {
        const {origin} = req.query;
        const {destination} = req.query;
        const {airline} = req.query;
        const {locale} = req.query;

        const token =  process.env.API_TOKEN;

        const allInclusive = await flightFilter.get_flights_special_offers(origin,destination,airline,locale,token);
       
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

module.exports = {
    tickets_for_specific_date,
    calendar_of_prices,
    cheapest_tickets,
    non_stop_tickets,
    flights_special_offers
}