var mysqlConnection = require('../Config/db');
const hotelFilter = require('../Helpers/hotel_filter');
const md5 = require('md5');
const express = require('express');
const app = express();
var geoip = require('geoip-lite');

const hotel_filter  = async(req,res) => 
{
       try {
        const {iata} = req.query;
        const {checkIn} = req.query;
        const {checkOut} = req.query;
        const {childAge1} = req.query;
        const {adultsCount} = req.query;
        const {childrenCount} = req.query;
        const marker =  process.env.API_MARKER;
        const apiToken =  process.env.API_TOKEN;

        const lang = 'en';
        const currency = 'USD';
        const waitForResult  = 0;
        const ipAddress = req.socket.remoteAddress;
        var ip = ipAddress.replace('::ffff:','');
        const customerIP = ip;  

        const sign = apiToken+':'+marker+':'+adultsCount+':'+checkIn+':'+checkOut+':'+childAge1+':'+childrenCount+':'+currency+':'+customerIP+':'+iata+':'+lang+':'+waitForResult;
        const signature = md5(sign);
        const allInclusive = await hotelFilter.get_hotel_searchid(iata,checkIn,checkOut,adultsCount,customerIP,childrenCount,childAge1,lang,currency,waitForResult,marker,signature,apiToken);
        let resp = JSON.parse(allInclusive);  
        let searchId = resp.searchId;
        console.log(searchId);
        const hotels = await hotelFilter.get_hotels(searchId,marker,apiToken);
          
        
       res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(hotels)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

module.exports = {
    hotel_filter
}