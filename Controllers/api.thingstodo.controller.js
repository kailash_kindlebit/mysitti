var mysqlConnection = require('../Config/db');
const yelpAPISCall = require('../Helpers/yelp_apis');
const categoryFilter = require('../Helpers/category_filter');
const iziTravelApis = require('../Helpers/izi_travel_apis');
const express = require('express');
const app = express();
var geoip = require('geoip-lite');
const get_local_music = (req,res) => 
{
    try {
        const sql = 'SELECT music_image,music_type FROM music_categories';
        mysqlConnection.query(sql, (err,localMusic) =>
        {
            if(!err)
            {
                 var resData = [];
                localMusic.forEach(element =>
                {
                    const {music_image,music_type} = element;
                    let initi = {
                    "music_type":music_type,"music_image":process.env.Base_url+'/assets/images/city_images/'+music_image
                    }
                    resData.push(initi);
                });
                // res.json({'status':true,"messagae":"data get successfully!","data":resData});
                res.json({'status':true,"messagae":"data get successfully!","localMusic":resData});
            } else {
                res.json({'status':false,"messagae":err});  
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_ip = async(req,res) => 
{ 
    const ipAddress = req.socket.remoteAddress;
    // var ip = ipAddress.replace('::ffff:','');
    var ip = '184.154.82.162';
    var geo = geoip.lookup(ip);
    if(geo)
    {
         res.json({'status':true,"ip":ip, "data":geo});
    } else {
        res.json({'status':false,"ip":'', "data":'chicago'});
    }
   
}

const get_family_top_attractions = async(req,res) => 
{
    try {
        // family top attractions 
        const {city} = req.query;
        let result = await yelpAPISCall.get_latitude_longitude(city);
        let latitude = result?.lat;
        let longitude = result?.lng;
        let response = await yelpAPISCall.yelp_api_call(latitude,longitude,3,'family attractions');
        let familytopattr = JSON.parse(response);
        res.json({'status':true,"messagae":"data get successfully!","familytopattr":familytopattr});
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_winery = async(req,res) => 
{
    try {
        // Winery
        const {city} = req.query;
        let result1 = await yelpAPISCall.get_latitude_longitude(city);
        let latitude1 = result1?.lat;
        let longitude1 = result1?.lng;
        let response1 = await yelpAPISCall.yelp_api_call(latitude1,longitude1,4,'Winery');
        console.log('response1',response1);
        let winery = JSON.parse(response1);
        res.json({'status':true,"messagae":"data get successfully!","winery":winery});
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_escape_room = async(req,res) => 
{
    try {
        // Winery
        const {city} = req.query;
        let result1 = await yelpAPISCall.get_latitude_longitude(city);
        let latitude1 = result1?.lat;
        let longitude1 = result1?.lng;
        let response1 = await yelpAPISCall.yelp_api_call(latitude1,longitude1,4,'escape-20-room');
        console.log('response1',response1);
        let escape = JSON.parse(response1);
        res.json({'status':true,"messagae":"data get successfully!","escape":escape});
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_comedy = async(req,res) => 
{
    try {
        // Comedy
        const {city} = req.query;
        let result2 = await yelpAPISCall.get_latitude_longitude(city);
        let latitude2 = result2?.lat;
        let longitude2 = result2?.lng;
        let response2 = await yelpAPISCall.yelp_api_call(latitude2,longitude2,4,'Comedy');
        let comedy = JSON.parse(response2);
        res.json({'status':true,"messagae":"data get successfully!","comedy":comedy});
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_adrenaline_rush = (req,res) => 
{
    try {
        const {city} = req.query;
        const adrenalinerush = "SELECT link,image_link,title,image_name FROM specific_adrenaline WHERE city1 like '%"+city+"%' or city2 like '%"+city+"%' limit 15";
        mysqlConnection.query(adrenalinerush, (er,adrenalineRush) => 
        {
            if(er) throw (er);
            res.json({'status':true,"messagae":"data get successfully!","adrenalineRush":adrenalineRush});
        })
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_tours_travel = (req,res) => 
{
    try {
        const {city} = req.query;
        const tourstravel = city == 'Washington D.C.' ? "SELECT * FROM  tours4fun_xml_listing WHERE city LIKE '%Washington%' AND tag = 'Tours4Fun' LIMIT 10" : "SELECT * FROM  tours4fun_xml_listing WHERE city LIKE '%"+city+"%' AND tag = 'Tours4Fun' LIMIT 10";
        mysqlConnection.query(tourstravel, (er,tourstravel) => 
        {
            if(er) throw (er);
            res.json({'status':true,"messagae":"data get successfully!","tourstravel":tourstravel});
        })
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_dig_into = (req,res) => 
{
    try {
        const {city} = req.query;
        const diginto = "SELECT * FROM  get_guide_tours WHERE city_name LIKE '%"+city+"%' LIMIT 1";
        mysqlConnection.query(diginto, (er,digInto) => 
        {
            if(er) throw (er);
            res.json({'status':true,"messagae":"data get successfully!","digInto":digInto});
        })
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_where_to_stay = (req,res) => 
{
    try {
        const {city} = req.query;
        const wheretostay = "SELECT content FROM specific_city_sidebar WHERE city like '%"+city+"%' limit 1";
        mysqlConnection.query(wheretostay, (er,whereToStay) => 
        {
            if(er) throw (er);
            res.json({'status':true,"messagae":"data get successfully!","whereToStay":whereToStay});
        })
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_sports_tickets = (req,res) => 
{
    try {
        const {city} = req.query;
        const sportstickets = "SELECT city_name,state_id FROM capital_city WHERE city_name = '"+city+"'";
        mysqlConnection.query(sportstickets,(e,sportsTickets) => 
        {
            if(e) throw (e);
            const sportstickets1 = "select country_id,name,code FROM zone where zone_id = '"+sportsTickets[0]?.state_id+"' and status ='1'";
            mysqlConnection.query(sportstickets1,(e1,sportsTickets1) => 
            {
                if(e1) throw (e1);  
                const sportstickets2  = "SELECT * FROM `sportsTeam` WHERE state_code = '"+sportsTickets1[0]?.code+"' OR state_name = '"+sportsTickets1[0]?.name+"'";
                mysqlConnection.query(sportstickets2, (e2,sportsTickets2) => 
                {
                    if(e2) throw (e2);
                    res.json({'status':true,"messagae":"data get successfully!","sportsTickets":sportsTickets2});
                });
            });
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_groupon  = async(req,res) => 
{
    try {
         const flag = req.query.flag;
         const {city} = req.query;
         const {keyword} = req.query;
         const keywords = city+' '+keyword;
         let {limit} = req.query;

         const allInclusive = await yelpAPISCall.get_all_inclusive_by_country(keywords , limit );
              res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_all_tours  = async(req,res) => 
{
    try {
         const flag = req.query.flag;
         const {city} = req.query;
         // const {keyword} = req.query;
         const keywords = city+' '+'tours';
         let {limit} = req.query;

         const allInclusive = await yelpAPISCall.get_all_tourForFun(keywords , limit );
         console.log(allInclusive)
              res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const get_yelp_deals  = async(req,res) => 
{
    try {
        const {city} = req.query;
        const {limit} = req.query;
        const {keyword} = req.query;
        // if(keyword == "Sightseeing"){
        //     const sql = 'SELECT * FROM  get_guide_tours WHERE city_name LIKE "%'+city+'%" LIMIT 1';
        //     mysqlConnection.query(sql,(err, result) => 
        //     {
        //         result.forEach(element =>
        //         {
        //             const {content} = element;
        //             res.json({'status':true,"messagae":"data get successfully!","data":content});
        //         });  
        //     });
        // }
        //const key  = 'family attractions';
        let result = await yelpAPISCall.get_latitude_longitude(city);
        let latitude = result?.lat;
        let longitude = result?.lng;
        let response = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,keyword);
        let familytopattr = JSON.parse(response);
        res.json({'status':true,"messagae":"data get successfully!","data":familytopattr});
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const category_filter  = async(req,res) => 
{
    try {
        const flag = req.query.flag;
        const {city} = req.query;
        const {keyword} = req.query;
        let limit = 15;
        // console.log(limit)
        const allInclusive = await categoryFilter.get_filter_category(city,keyword,limit );
        console.log(allInclusive);
              res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}



module.exports = {
    get_local_music,
    get_family_top_attractions,
    get_winery,
    get_all_tours,
    get_comedy,
    get_adrenaline_rush,
    get_tours_travel,
    get_dig_into,
    get_where_to_stay,
    get_sports_tickets,
    get_escape_room,
    get_groupon,
    get_yelp_deals,
    category_filter,
    get_ip
}