var mysqlConnection = require('../Config/db');
var mysqlSecConnection = require('../Config/secondary.db');
const yelpAPISCall = require('../Helpers/yelp_apis');
const iziTravelApis = require('../Helpers/izi_travel_apis');
const zomato = require('../Helpers/zomato');
var geoip = require('geoip-lite');
const request = require('postman-request');
const ticketmaster = require('../Helpers/ticketmaster');

const hotel_deals_search = async (req,res) => 
{
    try {
        const city = !req?.query?.city ? 'Chicago' : req?.query?.city.replace(' ','+');
        let limit =  10;
        let result = await yelpAPISCall.get_latitude_longitude(city);
        let latitude = result?.lat;
        let longitude = result?.lng;

        let deliveryRestaurant = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'delivery restaurants');
        let fineDining = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'fine dining');
        let modePricedRestaurants = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'moderately priced');
        let cheapEats = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'cheap eats');
        let breakfast = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'breakfast');
        // let foods = await yelpAPISCall.yelp_api_call(latitude,longitude,limit,'food');
        // define variable 
        let deliveryParse = JSON.parse(deliveryRestaurant);
        let fineParse = JSON.parse(fineDining);
        let modePricedParse = JSON.parse(modePricedRestaurants);
        let cheapParse = JSON.parse(cheapEats);
        let breakFastParse = JSON.parse(breakfast);
        // let food = JSON.parse(foods);
        // console.log(fineParse);
        let deliveryParseData = [];
        // let foodParseData = [];
        let fineDiningParseData = [];
        let modePricedRestaurantsParseData = [];
        let cheapEatsParseData = [];
        let breakfastParseData =[];

        // use for each loop for delivery Restaurant
        // if(food != '')
        // {
        //     food?.businesses.forEach(element =>
        //     {
        //         const {id,name,image_url,url,review_count,rating} = element;
        //         let initi = {
        //             "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
        //         }
        //         foodParseData.push(initi);
        //     });
        // }

        if(deliveryParse != '')
        {
            deliveryParse?.businesses.forEach(element =>
            {
                const {id,name,image_url,url,review_count,rating} = element;
                let initi = {
                    "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
                }
                deliveryParseData.push(initi);
            });
        }

        // use for each loop for fine Dining
        if(fineParse != '')
        {
            fineParse?.businesses.forEach(element =>
            {
                const {id,name,image_url,url,review_count,rating} = element;
                let initi = {
                    "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
                }
                fineDiningParseData.push(initi);
            });
        }

        // use for each loop for mode Priced Restaurants
        if(modePricedParse != '')
        {
            modePricedParse?.businesses.forEach(element =>
            {
                const {id,name,image_url,url,review_count,rating} = element;
                let initi = {
                    "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
                }
                modePricedRestaurantsParseData.push(initi);
            });
        }

        // use for each loop for cheap Eats
        if(cheapParse != '')
        {
            cheapParse?.businesses.forEach(element =>
            {
                const {id,name,image_url,url,review_count,rating} = element;
                let initi = {
                    "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
                }
                cheapEatsParseData.push(initi);
            });
        }

        // use for each loop for breakFast
        if(breakFastParse != '')
        {
            breakFastParse?.businesses.forEach(element =>
            {
                const {id,name,image_url,url,review_count,rating} = element;
                let initi = {
                    "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
                }
                breakfastParseData.push(initi);
            });
        }
        let resData = {
            "deliveryRestaurant":deliveryParseData,
            "fineDining":fineDiningParseData,
            "modePricedRestaurants":modePricedRestaurantsParseData,
            "cheapEats":cheapEatsParseData,
            "breakfast":breakfastParseData,
            // "food":foodParseData, 
        }
        res.json({'status':true,"messagae":"data get successfully!","data":resData});
    } catch (error) {
        console.log(error);
        res.json({'status':false,"messagae":error});
    }
}

const hotel_deals_filter_search =async (req,res) =>
{
    try {
        const {city,keyword,limit} = req.query;
        let limits =  limit || 15;
        let key = keyword.replace(' ','+');
        let result = await yelpAPISCall.get_latitude_longitude(city);
        let latitude = result?.lat;
        let longitude = result?.lng;
        let response = await yelpAPISCall.yelp_api_call(latitude,longitude,limits,key);
        let responseParse = JSON.parse(response);
        let resData = [];
        responseParse?.businesses.forEach(element =>
        {
            const {id,name,image_url,url,review_count,rating} = element;
            let initi = {
                "id":id,"name":name,"image_url":image_url,"url":url,"review_count":review_count,"rating":rating
            }
            resData.push(initi);
        });
        res.json({'status':true,"messagae":"data get successfully!","data":resData});
    } catch (error) {
        console.log(error);
        res.json({'status':false,"messagae":error});
    }
}

const zomatos =async (req,res) =>
{
    try {
        const {city} = req.query;
        let citys = city.replace(' ','+');
        let url = "https://developers.zomato.com/api/v2.1/locations?query="+citys+"";
        let result = await zomato.get_zomato(url);
        if(result){
        let responseParse = JSON.parse(result.body);
            var city_idd = responseParse.location_suggestions[0].city_id
        }else{
           var city_idd = '292'
        }
        var collection_url = "https://developers.zomato.com/api/v2.1/collections?city_id="+city_idd+"&count=15";
        let zomato_collections = await zomato.get_zomato(collection_url);
        let zomato_collections_final = JSON.parse(zomato_collections.body);
         res.json({'status':true,"messagae":"data get successfully!","data":zomato_collections_final.collections});
    } catch (error) {
        console.log(error);
        res.json({'status':false,"messagae":error});
    }
}

const get_izi_travel_apis =async (req,res) =>
{
    try {
        let offset = 0 ;
        let limit = offset == 5 ? 0 : 5;
        let city = req.query.city.replace(' ', '+');
        console.log(city);
        let url = 'https://api.izi.travel/mtg/objects/search?languages=en,nl&type=tour&query='+city+'&limit='+limit+'&offset='+offset;
        let response = await iziTravelApis.izi_travel_api_call(url);
        let responseParse = JSON.parse(response);
        var resData = [];
        var x=0;
        var resDatareq = [];
        // console.log(responseParse);
        responseParse?.forEach(async (element,index) =>
        {
            const {uuid,languages,map,content_provider,images,location,title} = element;
            let initi = {
                "uuid": uuid,
                "languages": languages[0],
                "map": map.bounds,
                "content_provider": content_provider.uuid,
                "images": images[0].uuid,
                "country_code": location.country_code,
                "country_uuid": location.country_uuid,
                "latitude": location.latitude,
                "longitude": location.longitude,
                "title": title
            }
            resData.push(initi);
            resDatareq[index]= initi;
            let tour_url =  "https://api.izi.travel/mtgobjects/"+uuid+"/children?languages=en,nl,ru&limit=15";
            let toursData = await iziTravelApis.izi_travel_api_call(tour_url);
            let toursDataParse = JSON.parse(toursData);
            let resTourData = [];
            toursDataParse?.forEach(items => 
            {
                const {uuid,trigger_zones,content_provider,images,title} = items;
                let image_uid = images ? images[0]?.uuid : null;
                let objData = {
                    "main_uiid":uuid,
                    // "circle_latitude":trigger_zones[0].circle_latitude,
                    // "circle_longitude":trigger_zones[0].circle_longitude,
                    "content_provider_uiid":content_provider.uuid,
                    "images_uiid": images ? images[0]?.uuid : null,
                    "tour_title":title,
                    "image_url":image_uid == null ? 'https://dev.mysittivacations.com/no-image.png' : 'https://media.izi.travel/'+content_provider.uuid+'/'+image_uid+'_800x600.jpg'
                }
                resTourData.push(objData);
            }); 
            resData[index]['imagesData'] = resTourData;
            if(responseParse.length-1== x)
            {
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
            }
            x++;
        });
    } catch (error) {
        console.log(error);
        res.json({'status':false,"messagae":error});  
    }
}

const get_izi_model_data =async (req,res) => 
{
    try {
        const uiid = req.query.main_uiid;
        let url = 'https://api.izi.travel/mtgobjects/'+uiid+'?languages=en,nl,ru';
        let response = await iziTravelApis.izi_travel_api_call(url);
        let responseParse = JSON.parse(response);
        let resData = {
            "content_provider_uuid": responseParse[0].content_provider.uuid,
            "audio_uiid" : responseParse[0].content[0].audio[0].uuid,
            "image_uiid" : responseParse[0].content[0].images[0].uuid,
            "description" : responseParse[0].content[0].desc,
            "title" : responseParse[0].content[0].title,
            "audio_url" : 'https://media.izi.travel/'+responseParse[0].content_provider.uuid+'/'+responseParse[0].content[0].audio[0].uuid+'.m4a',
            "image_url" : 'https://media.izi.travel/'+responseParse[0].content_provider.uuid+'/'+responseParse[0].content[0].images[0].uuid+'_800x600.jpg'
        }
        res.json({'status':true,"messagae":"data get successfully!","data":resData});
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const get_wp_blogs =(req,res) => 
{
    try {
        const sql = 'SELECT * FROM blogs ORDER BY created_at desc';
        mysqlConnection.query(sql,(err, result) => 
        {
            // console.log(result)
                let blogs = [];
                result.forEach(items => 
                 {
                    const {id,title,slug,short_description,long_description,city_name,seo_title,seo_keyword,image,created_at} = items;
                    let init = {
                        "id":id,
                        "title":title,
                        "slug":slug,
                        "name":title,
                        'date':created_at,
                        "image":"https://admin.mysittivacations.com/storage/"+image,
                        "post_content":long_description.replace(/(<([^>]+)>)/ig, '').substring(0, '300')
                       
                    }
                    blogs.push(init);

                 });
        res.json({'status':true,"messagae":"data get successfully!","data":blogs});
           
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const get_wp_blogs_with_filter =(req,res) => 
{
      const query = req.query.query;
    try {
        const sql = 'SELECT * FROM blogs WHERE title LIKE "%'+query+'%" ORDER BY created_at desc';
        mysqlConnection.query(sql,(err, result) => 
        {
            // console.log(result)
                let blogs = [];
                result.forEach(items => 
                 {
                    const {id,title,slug,short_description,long_description,city_name,seo_title,seo_keyword,image,created_at} = items;
                    let init = {
                        "id":id,
                        "title":title,
                        "slug":slug,
                        "name":title,
                        'date':created_at,
                        "image":"https://admin.mysittivacations.com/storage/"+image,
                        "post_content":long_description.replace(/(<([^>]+)>)/ig, '').substring(0, '300')
                       
                    }
                    blogs.push(init);

                 });
        res.json({'status':true,"messagae":"data get successfully!","data":blogs});
           
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const get_sightseeing_tours =(req,res) => 
{
    const city = req.query.city;
    try {
        const sql = 'SELECT * FROM  get_guide_tours WHERE city_name LIKE "%'+city+'%" LIMIT 1';
        mysqlConnection.query(sql,(err, result) => 
        {
            result.forEach(element =>
            {
                const {content} = element;
                res.json({'status':true,"messagae":"data get successfully!","data":content});
            });  
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const get_guide_tours =(req,res) => 
{
    const {city,limit} = req.query;
    try {
        const sql = 'SELECT * FROM  get_guide_tours WHERE city_name LIKE "%'+city+'%" LIMIT 1';
        const sqlTourXML = 'SELECT * FROM  tours4fun_xml_listing WHERE city LIKE "%'+city+'%" AND tag = "Tours4Fun" limit '+limit+'';
        let tourXML = [];
        let getYourGuide = [];
                var vacationsFinal = [];

        mysqlConnection.query(sql,(err, result) => 
        {
            result.forEach(element =>
            {
                const {content} = element;
                let initi = {
                "content":content
                }
                getYourGuide.push(initi);
            });

            // console.log(getYourGuide)
            mysqlConnection.query(sqlTourXML,(err,Vacations) => 
                {
                    if(!err)
                Vacations.forEach(items => 
                {
                    const {id,title,city,price,link,impression_url,image_link,tag} = items;
                    const afterequalto = link.slice(link.indexOf('=') + 1);
                    const remove3a = afterequalto.replace("%3A%2F%2F", "://")
                    const remove2f = remove3a.replace("%2F", "/")
                    const remove3f = remove2f.replace("%3F", "/")
                    const remove3d = remove3f.replace("%3D", "/")
                    const remove26 = remove3d.replace("%26", "/")

                    let init = {
                        "id":id,
                        "title":title,
                        "city":city,
                        "price":price,
                        "link":remove26,
                        "tag":tag,
                        "image_link":image_link,
                        "impression_url":impression_url
                    }
                    vacationsFinal.push(init);
                });
                let resData = {
                "tourSlider":vacationsFinal,
                "getYourGuide":getYourGuide,
                }
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
        });
           
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const get_wp_single_blog =(req,res) => 
{
    try {  
        const id = req.query.id;   
        const sql = 'SELECT * FROM blogs WHERE id='+id+'';
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var blogs = [];
                result.forEach(items => 
                {
                    const {id,title,long_description,city_name,seo_title,seo_keyword,image,created_at} = items;
                    let init = {
                        "id":id,
                        "title":title,
                        "seo_title":seo_title,
                        "seo_keyword":seo_keyword,
                        "name":title,
                        'date':created_at,
                        "image":"https://admin.mysittivacations.com/storage/"+image,
                        "post_content":long_description.replace(/(<([^>]+)>)/ig, '')
                    }
                    blogs.push(init);

                });
                res.json({'status':true,"messagae":"data get successfully!","data":blogs});
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const get_wp_single_blog_with_slug =(req,res) => 
{
    // console.log(req.params.slug)
    try {  
        const slug = req.params.slug;   
        const sql = 'SELECT * FROM blogs WHERE slug="'+slug+'"';
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var blogs = [];
                result.forEach(items => 
                {
                    const {id,title,long_description,seo_description,city_name,seo_title,seo_keyword,image,created_at} = items;
                    let init = {
                        "id":id,
                        "title":title,
                        "slug":slug,
                        "city":city_name,
                        "seo_title":seo_title,
                        "seo_keyword":seo_keyword,
                        "seo_description":seo_description,
                        "name":title,
                        'date':created_at,
                        "image":"https://admin.mysittivacations.com/storage/"+image,
                        "post_content":long_description
                    }
                    blogs.push(init);

                });
                res.json({'status':true,"messagae":"data get successfully!","data":blogs});
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const beautiful_cities =(req,res) => 
{
    try {  
        const city = req.query.city; 
        const sql = "SELECT * FROM  tours4fun_xml_listing WHERE city LIKE '%"+city+"%' AND tag = 'Tours4Fun' LIMIT 20";
        console.log(sql);
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err)
                res.json({'status':true,"messagae":"data get successfully!","data":result});
            else
                res.json({'status':false,"messagae":err});
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const specific_hotels =(req,res) => 
{
    try {  
        const city = req.query.city; 
        const sql = "SELECT content FROM specific_city_sidebar WHERE city like '%"+city+"%' limit 1";
        console.log(sql);
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var resData = [];
                result.forEach(element =>
                {
                    const {id,content} = element;
                    var removeLimit = content.replace('limit=50','limit=3')
                    var removePowerBy = removeLimit.replace('powered_by=true','powered_by=false')
                    let initi = {
                    "id":id,"content":removePowerBy
                    }
                    resData.push(initi);
                });
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const hotel_url =(req,res) => 
{
    try {  
        const city = req.query.city; 
        const sql = "SELECT city_id_code FROM new_random_deal_hotel_widgets WHERE city like '%"+city+"%' limit 1";
        // console.log(sql);
        var datetime = new Date();
        // var tomorrow = new Date(datetime.setDate(datetime.getDate() + 2))
        var finalDate = datetime.toISOString().slice(0,10)
        // console.log(new Date(finalDate));
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var resData = [];
                // console.log(result.length)
                if(result.length > 0){
                result.forEach(element =>
                {
                    const {city_id_code} = element;
                    var location_id_url = "https://travel.mysittivacations.com/hotels?adults=1&checkIn="+finalDate+"&checkOut="+finalDate+"&children=&currency=usd&locale=en&locationId="+city_id_code+"&marker=130544.Zz74bb75a04c984737b399c37-130544"
                   console.log(location_id_url);
                    let initi = {
                    "city_id_code":location_id_url
                    }
                    resData.push(initi);
                });
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
               } else{
                var dataFinal = "https://travel.mysittivacations.com/hotels?adults=1&checkIn="+finalDate+"&checkOut="+finalDate+"&children=&currency=usd&locale=en&locationId=18401&marker=130544.Zz74bb75a04c984737b399c37-130544"
                res.json({'status':true,"messagae":"data get successfully!","data":dataFinal});
                }
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const flight_url =(req,res) => 
{
   
    try {  
        const city = req.query.city; 
        var iataFinal = "";
        var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
        let url = "https://www.travelpayouts.com/whereami?locale=en&ip="+ip
        request(url, async function (error, response, body) 
        {
   
            let responses = JSON.parse(body);
            if(responses){
                iataFinal = responses.iata
            }else{
                iataFinal = "DFW"
            }
        // resolve(response);
             const sql = "SELECT code FROM city_iata_code WHERE name like '%"+city+"%' limit 1";
        // console.log(sql);
        var datetime = new Date();
        // var tomorrow = new Date(datetime.setDate(datetime.getDate() + 2))
        var finalDate = datetime.toISOString().slice(0,10)
        // console.log(new Date(finalDate));
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var resData = [];
                // console.log(result.length)
                if(result.length > 0){
                result.forEach(element =>
                {
                    const {code} = element;

// var location_id_url = "https://travel.mysittivacations.com/hotels?adults=1&checkIn="+finalDate+"&checkOut="+finalDate+"&children=&currency=usd&locale=en&locationId="+city_id_code+"&marker=130544.Zz74bb75a04c984737b399c37-130544"
var location_id_url = "https://travel.mysittivacations.com/flights?adults=1&children=0&currency=usd&depart_date="+finalDate+"&destination_iata="+code+"&infants=0&marker=130544.Zzb4aa0accf8fd4d43b39445a-130544&origin_iata="+iataFinal+"&return_date="+finalDate+"&trip_class=0&with_request=true"
      
                    let initi = {
                    "url":location_id_url
                    }
                    resData.push(initi);
                });
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
               } else{
                var dataFinal = "https://travel.mysittivacations.com/flights?adults=1&children=0&currency=usd&depart_date="+finalDate+"&destination_iata=CHI&infants=0&marker=130544.Zzb4aa0accf8fd4d43b39445a-130544&origin_iata="+iataFinal+"&return_date="+finalDate+"&trip_class=0&with_request=true"
                res.json({'status':true,"messagae":"data get successfully!","data":dataFinal});
                }
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
        
        });
  
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const flight_iata_code =(req,res) => 
{
   
    try {  
        const city = req.query.city; 
        const sql = "SELECT code FROM city_iata_code WHERE name like '%"+city+"%' limit 1";
        mysqlConnection.query(sql,(err, result) => {
            if(!err){
                if(result.length > 0){

                res.json({'status':true,"messagae":"data get successfully!","data":result[0].code});  
            }else{
                 res.json({'status':true,"messagae":"data get successfully!","data":""});  
            }
            }else{
                res.json({'status':true,"messagae":"data get successfully!","data":""});  

            }
        })
  
    } catch (error) {
        res.json({'status':true,"messagae":"data get successfully!","data":""});  
    }
}

const car_rental =(req,res) => 
{
    try {  
        const city = req.query.city; 
        if(city == 'Washington D.C.'){
            var trigger_city = 'washington';
        }else{
            var trigger_city = city;
        }
        var trigger_city = trigger_city.replace(' ', '-');
        const sql = "SELECT * FROM  discover_cars WHERE location LIKE '%"+trigger_city+"%' LIMIT 2";
        // console.log(sql);
        var i = 0;
        mysqlConnection.query(sql,(err, result) => 
        { 
            i++
            var iataCode = JSON.parse(JSON.stringify(result));
            var countData = iataCode.length
            if(countData > 0){
               var locations = iataCode[i]['location'].replace(' ','-')
               // console.log(locations)
                var data = '<script src="https://c117.travelpayouts.com/content?trs=26480&shmarker=130544&location='+locations+'&locale=en&currency=usd&powered_by=false&bg_color=FFFFFF&font_color=333333&button_color=63ab45&button_font_color=ffffff&button_text=Search&promo_id=3873" charset="utf-8" async="true"></script>'
            }else{
                var data = '<script src="https://c117.travelpayouts.com/content?trs=26480&shmarker=130544&location=usa-new-york%2Flong-island%2Flong-island-airport-isp&locale=en&currency=usd&powered_by=false&bg_color=FFFFFF&font_color=333333&button_color=63ab45&button_font_color=ffffff&button_text=Search&promo_id=3873" charset="utf-8" async="true"></script>'

            }
            // console.log(i)
            if(!err)
                res.json({'status':true,"messagae":"data get successfully!","data":data});
                else
                res.json({'status':false,"messagae":err});
            });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const all_tours = async(req,res) => 
{
    try { 
        const {city} = req.query;
        const {keyword} = req.query;
        const final = city+' '+keyword;
        let {limit} = req.query;
        const allInclusive = await yelpAPISCall.get_all_inclusive_by_country(final , limit );
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
    }catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const all_inclusive_deals = async(req,res) => 
{
    try { 
        const {city} = req.query;
        const {keyword} = req.query;
        let {limit} = req.query;
         const final = city+'-'+keyword;
        const allInclusive = await yelpAPISCall.get_all_tourForFun(final , limit );
        res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
    }catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

const ticketmasters = async(req,res) => 
{
    try { 
        const {city} = req.query;
        const {keyword} = req.query;
        const allInclusive = await ticketmaster.ticketmaster(keyword , city );
        // console.log(allInclusive)
        res.json({'status':true,"messagae":"data get successfully!","data":allInclusive._embedded.events});
    }catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const contact_us = async(req,res) => 
{
    // console.log(req.body.city)
      try { 
        const {fname} = req.body;
        const {lname} = req.body;
        const {email} = req.body;
        const {phone} = req.body;
        const {text} = req.body;
        var sql = "INSERT INTO contact_us (fname, lname,email,phone,message) VALUES ('"+fname+"', '"+lname+"','"+email+"','"+phone+"','"+text+"')";
        console.log(sql)
        mysqlConnection.query(sql, function (err, result) {
        if (err) throw err;
            res.json({'status':true,"messagae":"data get successfully!"});
        });
    }catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}
const ticketmasters_sidebar = async(req,res) => 
{
    try {  
        const city = req.query.city; 
        const sql = "SELECT city_name,state_id FROM capital_city WHERE city_name = '"+city+"'";
        mysqlConnection.query(sql,(err, result) => 
        {
            var state_id = JSON.parse(JSON.stringify(result));
            var state_id_final = state_id[0].state_id
            const state_name_query = "select country_id,name,code FROM zone where zone_id = '"+state_id_final+"' and status ='1'"
        mysqlConnection.query(state_name_query,(err, state_name_query_result) => 
        {
            // console.log(state_name_query_result);
            var state_result = JSON.parse(JSON.stringify(state_name_query_result));
            var state_code = state_result[0].code
            var state_name = state_result[0].name

        const sporteam = "SELECT NBA,NFL,NHL,MLB,MLS,CFL,Colleges FROM `sportsTeam` WHERE state_code = '"+state_code+"' OR state_name = '"+state_name+"'"
        mysqlConnection.query(sporteam,(err, sporteam_result) => 
        {
            sporteamArray = []
            // console.log(sporteam_result)
            if(!err){

                   sporteam_result.forEach(element =>
                {
                    NFLFirst = []
                    const {NBA,NFL,NHL,MLB,MLS,CFL,Colleges} = element;
                    if(NBA){
                       var NBAFirst = NBA
                        
                    } if(NFL){
                       var NFLFirst = NFL
                    } if(NHL){
                       var NHLFirst = NHL
                    } if(MLB){
                       var MLBFirst = MLB
                    } if(MLS){
                       var MLSFirst = MLS
                    } if(CFL){
                       var CFLFirst = CFL
                    }
                     if(Colleges){
                       var CollegesFirst = Colleges
                    }
                    let initi = {
                        "NBA":NBAFirst,"NFL":NFLFirst,"NHL":NHLFirst,"MLB":MLBFirst,"MLS":MLSFirst,"CFL":CFLFirst,'Colleges':CollegesFirst
                    }
                sporteamArray.push(initi);
                });

            res.json({'status':true,"messagae":"data get successfully!","data":sporteamArray});
           } else{
            res.json({'status':false,"messagae":err});
        }
        });  
        }); 
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }

}

const yelp_sports_ticket = async(req,res) => 
{
    try {  
        const city = req.query.city; 
        const sql = "SELECT city_name,state_id FROM capital_city WHERE city_name = '"+city+"'";
        mysqlConnection.query(sql,(err, result) => 
        {
            var state_id = JSON.parse(JSON.stringify(result));
            var state_id_final = state_id[0].state_id
            const state_name_query = "select country_id,name,code FROM zone where zone_id = '"+state_id_final+"' and status ='1'"
        mysqlConnection.query(state_name_query,(err, state_name_query_result) => 
        {
            // console.log(state_name_query_result);
            var state_result = JSON.parse(JSON.stringify(state_name_query_result));
            var state_code = state_result[0].code
            var state_name = state_result[0].name

        const sporteam = "SELECT NBA,NFL,NHL,MLB,MLS,CFL FROM `sportsTeam` WHERE state_code = '"+state_code+"' OR state_name = '"+state_name+"'"
        mysqlConnection.query(sporteam,(err, sporteam_result) => 
        {
            sporteamArray = [];
            // console.log(sporteam_result)
            if(!err){
                sporteam_result.forEach(element =>
                {
                    const {NBA,NFL,NHL,MLB,MLS,CFL} = element;
                    if(NBA){
                       var NFLFirst = NBA.split(',')[0]
                    } if(NFL){
                       var NFLFirst = NFL.split(',')[0]
                    } if(NHL){
                       var NHLFirst = NHL.split(',')[0]
                    } if(MLB){
                       var MLBFirst = MLB.split(',')[0]
                    } if(MLS){
                       var MLSFirst = MLS.split(',')[0]
                    } if(CFL){
                       var CFLFirst = CFL.split(',')[0]
                    }
                    let initi = {
                        "NBA":NFLFirst,"NFL":NFLFirst,"NHL":NHLFirst,"MLB":MLBFirst,"MLS":MLSFirst,"CFL":CFLFirst
                    }
                sporteamArray.push(initi);
                });
            res.json({'status':true,"messagae":"data get successfully!","data":sporteamArray});
            }else{
            res.json({'status':false,"messagae":err});
        }
        });  
        }); 
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }

}

module.exports = {
    hotel_deals_search,
    all_tours,
    ticketmasters_sidebar,
    get_wp_single_blog_with_slug,
    yelp_sports_ticket,
    flight_iata_code,
    zomatos,
    flight_url,
    ticketmasters,
    all_inclusive_deals,
    get_guide_tours,
    get_sightseeing_tours,
    hotel_url,
    get_wp_blogs_with_filter,
    car_rental,
    hotel_deals_filter_search,
    get_izi_travel_apis,
    specific_hotels,
    get_izi_model_data,
    get_wp_blogs,
    contact_us,
    beautiful_cities,
    get_wp_single_blog,
}