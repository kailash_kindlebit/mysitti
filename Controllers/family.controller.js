var mysqlConnection = require('../Config/db');
const familyFilter = require('../Helpers/family_filter');
const yelpAPISCall = require('../Helpers/yelp_apis');

const groupon_api_call  = async(req,res) => 
{
       try {
        const {city} = req.query;
        const {limit} = req.query;
        const {keyword} = req.query;
       
        let result = await familyFilter.get_latitude_longitude(city);
        let resp = JSON.parse(result);
        let latitude = resp.results[0].geometry.location.lat;
        let longitude = resp.results[0].geometry.location.lng;
        const allInclusive = await familyFilter.groupo_api_call(latitude,longitude,limit,keyword);
       
       res.json({'status':true,"messagae":"data get successfully!","data":JSON.parse(allInclusive)});
             
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const tours_and_travel = (req,res) => 
{
    try {
        const {city} = req.query;
        const {limit} = req.query;
        const tourstravel = city == 'Washington D.C.' ? "SELECT * FROM  tours4fun_xml_listing WHERE city LIKE '%Washington%' AND tag = 'Tours4Fun' LIMIT "+limit+"" : "SELECT * FROM  tours4fun_xml_listing WHERE city LIKE '%"+city+"%' AND tag = 'Tours4Fun' LIMIT "+limit+"";
        mysqlConnection.query(tourstravel, (er,tourstravel) => 
        {
            if(er) throw (er);
            res.json({'status':true,"messagae":"data get successfully!","tourstravel":tourstravel});
        })
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}



module.exports = {
    groupon_api_call,
    tours_and_travel
}