var mysqlConnection = require('../Config/db');
const yelpAPISCall = require('../Helpers/yelp_apis');
const categoryFilter = require('../Helpers/category_filter');
const iziTravelApis = require('../Helpers/izi_travel_apis');
const express = require('express');
var geoip = require('geoip-lite');
const request = require('postman-request');

const get_beach = (req,res) => 
{
    try {
        var datetime = new Date();

const today = new Date();
// console.log(today)
// console.log("today => ", today);
let tomorrow = new Date();
tomorrow.setDate(today.getDate() + 2);
// console.log(tomorrow)
        var finalDate1 = datetime.toISOString().slice(0,10)
        var finalDate2 = tomorrow.toISOString().slice(0,10)
        const sql = 'SELECT * FROM beach limit 20 offset 10';
        mysqlConnection.query(sql, (err,beach) =>
        {
            if(!err)
            {
                let resData = [];
                beach.forEach(element =>
                {
                    const {id,name,image_url} = element;


                    var hotel_url = 'https://travel.mysittivacations.com/hotels?=1&adults=2&checkIn='+finalDate1+'&checkOut='+finalDate2+'&children=&cityId=17922&currency=usd&destination='+name.split(',')[0].replace(" ", "+")+'&language=en_us&marker=130544.Zze3e563e649454e4a8849952-130544';
                    let initi = {
                        "id":id,"name":name,"image_url":process.env.Base_url+'/assets/'+image_url,"url":hotel_url
                    }
                    resData.push(initi);
                });

                res.json({'status':true,"messagae":"data get successfully!","data":resData});
            } else {
                res.json({'status':false,"messagae":err});  
            }
        });
    } catch (error) {
        res.json({'status':false,"messagae":error});
    }
}

const cool_flight_deals =(req,res) => 
{
    try { 
        const sql = "SELECT name,image_url,iata_code FROM popular_cities limit 20";
        var datetime = new Date();
        var finalDate = datetime.toISOString().slice(0,10)
         var iataFinal = "";
       
        var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
        let url = "https://www.travelpayouts.com/whereami?locale=en&ip="+ip

        request(url, async function (error, response, body) 
        {
   
            let responses = JSON.parse(body);
            if(responses){
                iataFinal = responses.iata
            }else{
                iataFinal = "DFW"
            }
       
        mysqlConnection.query(sql,(err, result) => 
        {
            if(!err){
                var resData = [];
                result.forEach(element =>
                {
                    const {name,image_url,iata_code} = element;
                    var location_url = "https://travel.mysittivacations.com/flights?adults=1&children=0&currency=usd&depart_date="+finalDate+"&destination_iata="+iata_code+"&infants=0&marker=130544.Zzb4aa0accf8fd4d43b39445a-130544&origin_iata="+iataFinal+"&return_date="+finalDate+"&trip_class=0&with_request=true"
                    let initi = {
                        "url":location_url,
                        "title":name,
                        "image_url":process.env.Base_url+'/assets/'+image_url
                    }
                    resData.push(initi);
                });
                res.json({'status':true,"messagae":"data get successfully!","data":resData});
             
            }else{
                res.json({'status':false,"messagae":err});
            }
        });
         });
    } catch (error) {
        res.json({'status':false,"messagae":error});  
    }
}

module.exports = {
    get_beach,
    cool_flight_deals
}