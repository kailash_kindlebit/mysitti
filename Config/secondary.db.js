const mysql = require('mysql');

const DB_HOST = process.env.DB_HOST
const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD
const DB_DATABASE_SEC = process.env.DB_DATABASE_SEC

var secondaryConnection = mysql.createConnection({
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE_SEC,
    multipleStatements: true,
    dateStrings: true
});

secondaryConnection.connect((err)=> {
    if(!err)
        console.log('DB 2 Connection Established Successfully');
    else
        console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
});

module.exports = secondaryConnection;